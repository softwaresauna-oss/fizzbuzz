const doFizzBuzz = (value: number): string => {

    if (value % 15 === 0) {
        return 'fizzbuzz';
    } else if (value % 3 === 0) {
        return 'fizz';
    } else if (value % 5 === 0) {
        return 'buzz';
    } else {
        return value.toString();
    }
};

describe('numbers divisible by 3 and not by 5 return fizz', () => {
    [3, 6, 9].forEach(value =>
        test(`value: ${value}`, () => {
            const result = doFizzBuzz(value);

            expect(result).toBe('fizz');
        }));
});

describe('numbers divisible by 5 and not by 3 return buzz', () => {
    [5, 10, 20].forEach(value =>
        test(`value: ${value}`, () => {
            const result = doFizzBuzz(value);

            expect(result).toBe('buzz');
        }));
});

describe('numbers divisible by both 5 and 3 return fizzbuzz', () => {
    [15, 30, 45, 60].forEach(value =>
        test(`value: ${value}`, () => {
            const result = doFizzBuzz(value);

            expect(result).toBe('fizzbuzz');
        }));
});

describe('numbers not divisible by either 3 or 5 should return that number', () => {

    test.each`
        input |  output
        ${1}  |  ${'1'}
        ${2}  |  ${'2'}
        ${4}  |  ${'4'}
        ${7}  |  ${'7'}
    `('$input -> $output', ({ input, output }) => {

        expect(doFizzBuzz(input)).toBe(output);
    });
});
